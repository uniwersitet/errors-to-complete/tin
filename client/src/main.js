import Vue from 'vue';
import VueCryptoJs from 'vue-cryptojs';
import App from './App.vue';
import router from './router';

Vue.config.productionTip = true;

Vue.use(VueCryptoJs);

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
