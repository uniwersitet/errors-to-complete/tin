import Vue from 'vue';
import VueRouter from 'vue-router';
import defaultFront from '../views/defaultFront.vue';
import defaultAdmin from '../views/defaultAdmin.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: defaultFront,
  },
  {
    path: '/admin',
    name: 'admin',
    secure: true,
    component: defaultAdmin,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
