var sqlite3 = require('sqlite3').verbose()
var md5 = require('md5')

const DBSOURCE = "db.sqlite"

let db = new sqlite3.Database(DBSOURCE, (err) => {
    if (err) {
        // Cannot open database
        console.error(err.message);
        throw err;
    } else {
        console.log('Connected to the SQLite database.');
    }
});

db.run(`-- Table: Country
        CREATE TABLE IF NOT EXISTS Country (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            country TEXT NOT NULL,
            alfa2 TEXT NOT NULL
        );`, (err) => {
    if (err) {
        console.log(err);
    }
});


db.run(`-- Table: Parcel
        CREATE TABLE IF NOT EXISTS Parcel (
            trackingId TEXT PRIMARY KEY,
            weigth REAL,
            deliveryTry INTEGER DEFAULT 0,
            sentDate DATE NOT NULL,
            dueAmount REAL DEFAULT 0.00,
            isPayed INTEGER DEFAULT 1
        )WITHOUT ROWID;`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`-- Table: Region
        CREATE TABLE IF NOT EXISTS Region (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            region TEXT NOT NULL,
            countryId INTEGER NOT NULL,
            CONSTRAINT Region_Country FOREIGN KEY (countryId)
            REFERENCES Country (id)
        );`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`-- Table: City
        CREATE TABLE IF NOT EXISTS City (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            city TEXT NOT NULL,
            regionId INTEGER NOT NULL,
            CONSTRAINT City_Region FOREIGN KEY (regionId) REFERENCES Region (id)
        );`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`-- Table: ZipCode
        CREATE TABLE IF NOT EXISTS ZipCode (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            zipCode TEXT NOT NULL,
            cityId INTEGER NOT NULL,
            CONSTRAINT ZipCode_City FOREIGN KEY (cityId)
            REFERENCES City (id)
        );`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`-- Table: Recipient
        CREATE TABLE IF NOT EXISTS Recipient (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            lastName TEXT NOT NULL,
            email TEXT NOT NULL,
            phone TEXT,
            zipCodeId INTEGER NOT NULL,
            street TEXT NOT NULL,
            CONSTRAINT Recipient_ZipCode FOREIGN KEY (zipCodeId)
            REFERENCES ZipCode (id)
        );`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`-- Table: User
        CREATE TABLE IF NOT EXISTS Users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            userName TEXT NOT NULL,
            password TEXT NOT NULL,
            auth TEXT NOT NULL,
            recipientId INTEGER NOT NULL,
            CONSTRAINT User_Recipient FOREIGN KEY (recipientId)
            REFERENCES Recipient (id)
        );`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`-- Table: Delivery
        CREATE TABLE IF NOT EXISTS Delivery (
            trackingId TEXT PRIMARY KEY,
            senderId INTEGER NOT NULL,
            recipientId INTEGER NOT NULL,
            note TEXT,
            status TEXT NOT NULL,
            type TEXT NOT NULL,
            CONSTRAINT Delivery_User FOREIGN KEY (senderId)
            REFERENCES Users (id),
            CONSTRAINT Delivery_Recipient FOREIGN KEY (recipientId)
            REFERENCES Recipient (id),
            CONSTRAINT Delivery_Parcel FOREIGN KEY (trackingId)
            REFERENCES Parcel (trackingId)
        )WITHOUT ROWID;`, (err) => {
    if (err) {
        console.log(err);
    }
});


db.run(`-- Indexes
        CREATE INDEX IF NOT EXISTS Country_alfa_idx
        ON Country (alfa2 ASC);`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`CREATE INDEX IF NOT EXISTS Parcel_idx_date
        ON Parcel (sentDate ASC);`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`CREATE INDEX IF NOT EXISTS Parcel_idx_payed
        ON Parcel (isPayed ASC);`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`CREATE INDEX IF NOT EXISTS Recipient_idx_mail
        ON Recipient (email ASC);`, (err) => {
    if (err) {
        console.log(err);
    }
});

db.run(`CREATE INDEX IF NOT EXISTS Recipient_idx_phone
        ON Recipient (phone ASC);`, (err) => {
    if (err) {
        console.log(err);
    }
});

module.exports = db;