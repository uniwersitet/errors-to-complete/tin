-- Table: Country
CREATE TABLE IF NOT EXISTS Country (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    country TEXT NOT NULL,
    alfa2 TEXT NOT NULL
);

-- Table: Parcel
CREATE TABLE IF NOT EXISTS Parcel (
    trackingId INTEGER PRIMARY KEY,
    weigth REAL NOT NULL,
    deliveryTry INTEGER,
    sentDate TEXT  NOT NULL,
    dueAmount REAL NOT NULL DEFAULT 0.00,
    isPayed INTEGER NOT NULL DEFAULT 0
);

-- Table: Region
CREATE TABLE IF NOT EXISTS Region (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    region TEXT NOT NULL,
    Country_id INTEGER NOT NULL,
    CONSTRAINT Region_Country FOREIGN KEY (Country_id)
    REFERENCES Country (id)
);

-- Table: City
CREATE TABLE IF NOT EXISTS City (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    city TEXT NOT NULL,
    Region_id INTEGER NOT NULL,
    CONSTRAINT City_Region FOREIGN KEY (Region_id) REFERENCES Region (id)
);

-- Table: ZipCode
CREATE TABLE IF NOT EXISTS ZipCode (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    zipCode TEXT NOT NULL,
    City_id INTEGER NOT NULL,
    CONSTRAINT ZipCode_City FOREIGN KEY (City_id)
    REFERENCES City (id)
);

-- Table: Recipient
CREATE TABLE IF NOT EXISTS Recipient (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT NOT NULL,
    lastName TEXT NOT NULL,
    email TEXT NOT NULL,
    phone TEXT,
    ZipCode_id INTEGER NOT NULL,
    street TEXT NOT NULL,
    CONSTRAINT Recipient_ZipCode FOREIGN KEY (ZipCode_id)
    REFERENCES ZipCode (id)
);      

-- Table: User
CREATE TABLE IF NOT EXISTS User (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    userName TEXT NOT NULL,
    password TEXT NOT NULL,
    auth TEXT NOT NULL,
    Recipient_id INTEGER NOT NULL,
    CONSTRAINT User_Recipient FOREIGN KEY (Recipient_id)
    REFERENCES Recipient (id)
);

-- Table: Delivery
CREATE TABLE IF NOT EXISTS Delivery (
    Parcel_trackingId INTEGER PRIMARY KEY,
    Sender_id INTEGER NOT NULL,
    Recipient_id INTEGER NOT NULL,
    note TEXT,
    status TEXT NOT NULL,
    type TEXT NOT NULL,
    CONSTRAINT Delivery_User FOREIGN KEY (Sender_id)
    REFERENCES User (id),
    CONSTRAINT Delivery_Recipient FOREIGN KEY (Recipient_id)
    REFERENCES Recipient (id),
    CONSTRAINT Delivery_Parcel FOREIGN KEY (Parcel_trackingId)
    REFERENCES Parcel (trackingId)
);


-- Indexes
CREATE INDEX Country_alfa_idx
ON Country (alfa2 ASC)
;

CREATE INDEX Parcel_idx_date
ON Parcel (sentDate ASC)
;
        
CREATE INDEX Parcel_idx_payed
ON Parcel (isPayed ASC)
;

CREATE INDEX Recipient_idx_mail
ON Recipient (email ASC)
;
        
CREATE INDEX Recipient_idx_phone
ON Recipient (phone ASC)
;


INSERT INTO COUNTRY (country, alfa2) VALUES ('Poland','PL');
INSERT INTO COUNTRY (country, alfa2) VALUES ('Italy','IT');
