const alfa = /'.*\d*.*'/

class Region {

    constructor(region, countryId) {
        this.region = region;
        this.countryId = countryId;
    }

    static regionValidator(region) {
        var err = [];
        if (!region.region || alfa.test(region.region)) {
            err.push('region');
        }

        if (!region.countryId) {
            err.push('country');
        }

        return err;
    }

}

module.exports = Region;