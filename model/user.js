const authe = ['admin', 'user'];

class User {

    constructor(username, pass, auth, recipientId) {
        this.username = username;
        this.pass = pass;
        this.auth = auth;
        this.recipientId = recipientId;
    }

    static userValidator(user) {
        var err = [];
        if (!user.username) {
            err.push('username');
        }

        if (!user.pass) {
            err.push('pass');
        }

        if (!user.auth || !authe.includes(user.auth)) {
            err.push('auth');
            console.log(authe.includes(user.auth));
        }

        if (!user.recipientId) {
            err.push('recipientId');
        }

        return err;
    }

}

module.exports = User;