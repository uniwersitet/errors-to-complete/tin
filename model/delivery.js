const stat = ["processing", "processed", "in progress", "delivered", "rejected"];
const typ = ["standard", "prioritary", "international"];

class Delivery {

    constructor(trackingId, senderId, recipientId, status, type, note) {
        this.trackingId = trackingId;
        this.senderId = senderId;
        this.recipientId = recipientId;
        this.note = note;
        this.status = status;
        this.type = type
    }

    static deliveryValidator(delivery) {
        var err = [];
        if (!delivery.trackingId) {
            err.push('tracking');
        }

        if (!delivery.senderId) {
            err.push('sender');
        }

        if (!delivery.recipientId) {
            err.push('recipient');
        }

        if (!delivery.status || !stat.includes(delivery.status)) {
            err.push('status');
        }

        if (!delivery.type || !typ.includes(delivery.type)) {
            err.push('type');
        }

        return err;
    }

}

module.exports = Delivery;