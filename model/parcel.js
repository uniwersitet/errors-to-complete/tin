const reg = /'[A-Z]{2,5}\d.*[A-Z]{0,2}'/;

class Parcel {

    constructor(trackingId, weight, sentdate, deliveryTry, dueAmount, isPayed) {
        this.trackingId = trackingId;
        this.weight = weight;
        this.deliveryTry = deliveryTry;
        this.sentdate = sentdate;
        this.dueAmount = dueAmount;
        this.isPayed = isPayed;
    }

    static parcelValidator(parcel) {
        var err = [];
        if (!parcel.trackingId || reg.test(parcel.trackingId)) {
            err.push('tracking');
        }

        if (!parcel.weight) {
            err.push('weigth');
        }

         if (!parcel.sentdate) {
             err.push('date');
             console.log(parcel.sentdate);
        }

        return err;
    }

}

module.exports = Parcel