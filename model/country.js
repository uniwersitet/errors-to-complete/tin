const alfa = /'.*\d*.*'/

class Country {

    constructor(country, alfa2) {
        this.country = country;
        this.alfa2 = alfa2;
    }

    static countryValidator(country) {
        var err = [];
        if (!country.country || alfa.test(country.country)) {
            err.push('country');
        }

        if (!country.alfa2 || country.alfa2.length > 2 ||
            country.alfa2.length < 2 || alfa.test(country.alfa2)) {
            err.push('alfa');
        }

        return err;
    }

}

module.exports = Country;