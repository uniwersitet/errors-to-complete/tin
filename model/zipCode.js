const alfa = /'[0-9|A-Z]{2,5}([\-| ][0-9|A-Z]{3,4})'/

class ZipCode {

    constructor(zipCode, cityId) {
        this.zipCode = zipCode;
        this.cityId = cityId;
    }

    static zipCodeValidator(zipCode) {
        var err = [];
        if (!zipCode.zipCode || zipCode.zipCode.length < 5 ||
            alfa.test(zipCode.zipCode)) {
            err.push('zipCode');
        }

        if (!zipCode.cityId) {
            err.push('city');
        }

        return err;
    }

}

module.exports = ZipCode;