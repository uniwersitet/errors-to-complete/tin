const alfa = /'.*\d*.*'/

class City {

    constructor(city, regionId) {
        this.city = city;
        this.regionId = regionId;
    }

    static cityValidator(city) {
        var err = [];
        if (!city.city || alfa.test(city.city)) {
            err.push('city');
        }

        if (!city.regionId) {
            err.push('regionId');
        }

        return err;
    }

}

module.exports = City;