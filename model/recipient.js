const alfa = /'.*\d+.*'/
const mail = /'[A-Z]|[0-9]|\.|_|%|\+|-+@[A-Z]|[0-9]|\.|-+\.[A-Z]{2,}'/
class Recipient {

    constructor(name, lastName, email, phone, zipCodeId, street) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.zipCodeId = zipCodeId;
        this.street = street;
    }

    static recipientValidator(recipient) {
        var err = [];

        if (!recipient.name || alfa.test(recipient.name)) {
            err.push('name');
        }

        if (!recipient.lastName || alfa.test(recipient.lastName)) {
            err.push('last name');
        }

        if (!recipient.email || !mail.test(recipient.email)) {
            err.push('email');
        }

        if (!recipient.phone) {
            err.push('phone');
        }

        if (!recipient.zipCodeId) {
            err.push('zipCodeId');
        }

        if (!recipient.street) {
            err.push('street');
        }

        return err;
    }

}

module.exports = Recipient;