const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const mailer = require('nodemailer');
const db = require("./db/database");

var app = express();

const countryService = require('./api/countryService');
const cityService = require('./api/cityService');
const deliveryService = require('./api/deliveryService');
const parcelService = require('./api/parcelService');
const recipientService = require('./api/recipientService');
const regionService = require('./api/regionService');
const userService = require('./api/userService');
const zipCodeService = require('./api/zipCodeService');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api/cities', cityService.route);
app.use('/api/countries', countryService.route);
app.use('/api/deliveries', deliveryService.route);
app.use('/api/parcels', parcelService.route);
app.use('/api/recipients', recipientService.route);
app.use('/api/regions', regionService.route);
app.use('/api/users', userService.route);
app.use('/api/zipCodes', zipCodeService.route);

app.post('/send-email', function (req, res) {
    let transporter = mailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            // should be replaced with real sender's account
            user: 'ChristianCitterio18@gmail.com',
            pass: 'Martufello1'
        }
    });
    let mailOptions = {
        // should be replaced with real recipient's account
        to: req.body.to,
        subject: req.body.subject,
        text: req.body.message
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
    });
    res.end();
  });

app.get('/', (req, res) => {
    res.json({
        message: 'Behold The MEVN Stack!'
    });
});

const port = process.env.PORT || 4000;
app.listen(port, () => {
    console.log(`listening on ${port}`);
});