const express = require('express');
const router = express.Router();
const User = require('../model/user');
const db = require('../db/database');


router.get('/', (req, res) => {
    let sql = `SELECT a.id, a.username, a.password, a.auth, 
        b.name, b.lastName, b.email, b.phone, b.street 
        FROM Users a
        JOIN Recipient b on a.recipientId = b.id;`;
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(rows);
    });
});

router.get('/:userId', (req, res) => {
    const id = req.params.userId;
    let sql = `select * from Users where id = ?;`;
    db.get(sql, id, (err, row) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(row);
    });
});

router.post('/', (req, res, next) => {
    var data = new User(req.body.userName, req.body.password,
        req.body.auth, req.body.recipientId);
    var errors = User.userValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `INSERT INTO Users (username, password, auth, recipientId) VALUES (?,?,?,?);`;
        var params = [data.username, data.pass, data.auth, data.recipientId];
        db.run(sql, params, function(err, result) {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.status(201).json(result);
        });
    }
});

router.patch('/:userId', (req, res, next) => {
    var id = req.params.userId;
    var data = new User(req.body.userName, req.body.password,
        req.body.auth, req.body.recipientId);
    var errors = User.userValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    }
    let sql = `UPDATE Users set 
        userName = ?,
        password = ?,
        auth = ?,
        recipientId = ?
        WHERE id = ?;`;
    var params = [data.username, data.pass, data.auth, data.recipientId, id];
    db.run(sql, params, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message });
            return;
        }
        res.json({
            message: 'success',
            data: data,
            changes: this.changes
        });
    });
});

router.delete('/:userId', (req, res, next) => {
    const id = req.params.userId
    let sql = `DELETE FROM Users WHERE id = ?;`;
    db.run(sql, id, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message })
            return;
        }
        res.json({ 'message': 'deleted', changes: this.changes })
    });
});

router.get('/username/:username', (req, res) => {
    const mail = req.params.username;
    let sql = `select * from Users where username = ?;`;
    db.get(sql, mail, (err, row) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(row);
    });
});
module.exports.route = router;