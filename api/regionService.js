const express = require('express');
const router = express.Router();
const Region = require('../model/region');
const db = require('../db/database');


router.get('/', (req, res) => {
    let sql = `SELECT a.id, a.region, b.country FROM Region a 
        JOIN Country b on b.id = a.countryId;`;
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(rows);
    });
});

router.get('/:regionId', (req, res) => {
    const id = req.params.regionId;
    let sql = `select * from Region where id = ?;`;
    db.get(sql, id, (err, row) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(row);
    });
});

router.post('/', (req, res, next) => {
    var data = new Region(req.body.region, req.body.countryId);
    var errors = Region.regionValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `INSERT INTO REGION (region, countryId) VALUES (?,?);`;
        var params = [data.region, data.countryId];
        db.run(sql, params, function(err, result) {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.status(201).json(result);
        });
    }
});

router.patch('/:regionId', (req, res, next) => {
    var id = req.params.regionId;
    var data = new Region(req.body.region, req.body.countryId);
    var errors = Region.regionValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `UPDATE Region set 
            region = ?,
            countryId = ?
            WHERE id = ?;`;
        var params = [data.region, data.countryId, id];
        db.run(sql, params, (err, result) => {
            if (err) {
                res.status(500).json({ 'error': res.message });
                return;
            }
            res.json({
                message: 'success',
                data: data,
                changes: this.changes
            });
        });
    }
});

router.delete('/:regionId', (req, res, next) => {
    const id = req.params.regionId
    let sql = `DELETE FROM region WHERE id = ?;`;
    db.run(sql, id, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message })
            return;
        }
        res.json({ 'message': 'deleted', changes: this.changes })
    });
});

module.exports.route = router;