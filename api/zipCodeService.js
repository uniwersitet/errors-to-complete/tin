const express = require('express');
const router = express.Router();
const ZipCode = require('../model/zipCode');
const db = require('../db/database');


router.get('/', (req, res) => {
    let sql = `SELECT ZipCode.id, ZipCode.zipCode, City.city FROM ZipCode
        JOIN City on zipCode.cityId = City.id;`;
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(rows);
    });
});

router.get('/:zipCodeId', (req, res) => {
    const id = req.params.zipCodeId;
    let sql = `select * from ZipCode WHERE id = ?;`;
    db.get(sql, id, (err, row) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(row);
    });
});

router.post('/', (req, res, next) => {
    var data = new ZipCode(req.body.zipCode, req.body.cityId);
    var errors = ZipCode.zipCodeValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `INSERT INTO ZipCode (zipCode, cityId) VALUES (?,?);`;
        var params = [data.zipCode, data.cityId];
        db.run(sql, params, function(err, result) {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.status(201).json(result);
        });
    }
});

router.patch('/:zipCodeId', (req, res, next) => {
    var id = req.params.zipCodeId;
    var data = new ZipCode(req.body.zipCode, req.body.cityId);
    var errors = ZipCode.zipCodeValidator(data);
    if (zipCode.length) {
        res.status(500).json(errors)
    }
    let sql = `UPDATE ZipCode set 
        zipCode = ?,
        cityId = ?
        WHERE id = ?;`;
    var params = [data.zipCode, data.cityId, id];
    db.run(sql, params, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message });
            return;
        }
        res.json({
            message: 'success',
            data: data,
            changes: this.changes
        });
    });
});

router.delete('/:zipCodeId', (req, res, next) => {
    const id = req.params.zipCodeId
    let sql = `DELETE FROM ZipCode WHERE id = ?;`;
    db.run(sql, id, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message })
            return;
        }
        res.json({ 'message': 'deleted', changes: this.changes })
    });
});

module.exports.route = router;