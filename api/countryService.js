const express = require('express');
const router = express.Router();
const Country = require('../model/country');
const db = require('../db/database');


router.get('/', (req, res) => {
    let sql = `SELECT * FROM Country;`;
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(rows);
    });
});

router.get('/:countryId', (req, res) => {
    const id = req.params.countryId;
    let sql = `select * from Country where id = ?;`;
    db.get(sql, id, (err, row) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(row);
    });
});

router.post('/', (req, res, next) => {
    var data = new Country(req.body.country, req.body.alfa2);
    var errors = Country.countryValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `INSERT INTO COUNTRY (country, alfa2) VALUES (?,?);`;
        var params = [data.country, data.alfa2];
        db.run(sql, params, function(err, result) {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.status(201).json(result);
        });
    }
});

router.patch('/:countryId', (req, res, next) => {
    var id = req.params.countryId;
    var data = new Country(req.body.country, req.body.alfa2);
    var errors = Country.countryValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `UPDATE Country set 
            country = ?,
            alfa2 = ?
            WHERE id = ?;`;
        var params = [data.country, data.alfa2, id];
        db.run(sql, params, (err, result) => {
            if (err) {
                res.status(500).json({ 'error': res.message });
                return;
            }
            res.json({
                message: 'success',
                data: data,
                changes: this.changes
            });
        });
    }
});

router.delete('/:countryId', (req, res, next) => {
    const id = req.params.countryId
    let sql = `DELETE FROM Country WHERE id = ?;`;
    db.run(sql, id, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message })
            return;
        }
        res.json({ 'message': 'deleted', changes: this.changes })
    });
});

module.exports.route = router;