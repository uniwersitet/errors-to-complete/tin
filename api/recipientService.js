const express = require('express');
const router = express.Router();
const Recipient = require('../model/recipient');
const db = require('../db/database');


router.get('/', (req, res) => {
    let sql = `SELECT * FROM Recipient;`;
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(rows);
    });
});

router.get('/:recipientId', (req, res) => {
    const id = req.params.recipientId;
    let sql = `select * from Recipient where id = ?;`;
    db.get(sql, id, (err, row) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(row);
    });
});

router.post('/', (req, res, next) => {
    var data = new Recipient(req.body.name, req.body.lastName, 
        req.body.email, req.body.phone, req.body.zipCodeId, req.body.street);
    var errors = Recipient.recipientValidator(data);
    if (errors.length) {
        res.status(500).json(errors);
    } else {
        let sql = `INSERT INTO Recipient (name, lastName, email, phone, zipCodeId, street) 
            VALUES (?,?,?,?,?,?);`;
        var params = [data.name, data.lastName, data.email, data.phone, data.zipCodeId, data.street];
        db.run(sql, params, function(err, result) {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.status(201).json(result);
        });       
    }
});

router.patch('/:recipientId', (req, res, next) => {
    var id = req.params.recipientId;
    var data = new Recipient(req.body.name, req.body.lastName, 
        req.body.email, req.body.phone, req.body.zipCodeId, req.body.street);
    var errors = Recipient.recipientValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `UPDATE Recipient set 
            name = ?,
            lastName = ?,
            email = ?,
            phone = ?,
            zipCodeId = ?,
            street = ?
            WHERE id = ?;`;
        var params = [data.name, data.lastName, data.email, 
            data.phone, data.zipCodeId, data.street, id];
        db.run(sql, params, (err, result) => {
            if (err) {
                res.status(500).json({ 'error': res.message });
                return;
            }
            res.json({
                message: 'success',
                data: data,
                changes: this.changes
            });
        });
    }
});

router.delete('/:recipientId', (req, res, next) => {
    const id = req.params.recipientId
    let sql = `DELETE FROM Recipient WHERE id = ?;`;
    db.run(sql, id, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message })
            return;
        }
        res.json({ 'message': 'deleted', changes: this.changes })
    });
});

module.exports.route = router;