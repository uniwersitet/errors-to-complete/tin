const express = require('express');
const router = express.Router();
const Parcel = require('../model/parcel');
const db = require('../db/database')

router.get('/', (req, res) => {
    let sql = `SELECT * FROM Parcel;`;
    var params = []
    db.serialize(() => {
        db.all(sql, params, (err, rows) => {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.json(rows);
        });
    });
});

router.get('/:parcelId', (req, res) => {
    const id = req.params.parcelId;
    let sql = `select * from Parcel where trackingId = ?;`;
    db.serialize(() => {
        db.get(sql, id, (err, row) => {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.json(row);
        });
    });
});

router.post('/', (req, res, next) => {
    var data = new Parcel(req.body.trackingId, req.body.weigth, req.body.sentDate,
        req.body.deliveryTry, req.body.dueAmount, req.body.isPayed);
    var errors = Parcel.parcelValidator(data)
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `INSERT INTO Parcel (trackingId, weigth, deliveryTry, sentDate, dueAmount, isPayed) 
                    VALUES (?,?,?,?,?,?);`;
        var params = [data.trackingId, data.weight,
            data.deliveryTry, data.sentdate,
            data.dueAmount, data.isPayed
        ];
        db.serialize(() => {
            db.run(sql, params, function(err, result) {
                if (err) {
                    res.status(400).json({ 'error': err.message });
                    return;
                }
                res.status(201).json(result);
            });
        });
    }
});

router.patch('/:parcelId', (req, res, next) => {
    var id = req.params.parcelId;
    var data = new Parcel(id, req.body.weigth,
        req.body.deliveryTry, req.body.sentDate,
        req.body.dueAmount, req.body.isPayed);
    var errors = Parcel.parcelValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `UPDATE Parcel set 
            trackingId = ?,
            weigth = ?,
            deliveryTry = ?,
            sentDate = ?,
            dueAmount = ?,
            isPayed = ?
            WHERE id = ?;`;
        var params = [data.trackingId, data.weight,
            data.deliveryTry, data.sentdate,
            data.dueAmount, data.isPayed, id
        ]
        db.serialize(() => {
            db.run(sql, params, (err, result) => {
                if (err) {
                    res.status(500).json({ 'error': res.message });
                    return;
                }
                res.status(201).json({
                    message: 'success',
                    data: data,
                    changes: this.changes
                });
            });
        });
    }
});

router.delete('/:parcelId', (req, res, next) => {
    const id = req.params.parcelId;
    let sql = `DELETE FROM Parcel WHERE trackingId = ?;`;
    db.serialize(() => {
        db.run(sql, id, (err, result) => {
            if (err) {
                res.status(500).json({ 'error': res.message });
                return;
            }
            res.status(201).json({ 'message': 'deleted', changes: this.changes });
        });
    });
});

module.exports.route = router;