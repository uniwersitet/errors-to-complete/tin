const express = require('express');
const router = express.Router();
const Delivery = require('../model/delivery');
const db = require('../db/database');


router.get('/', (req, res) => {
    let sql = `SELECT a.trackingId, b.sentDate, a.status, 
        a.type, b.dueAmount, b.isPayed
        FROM Delivery a
        JOIN Parcel b ON a.trackingId = b.trackingId;`;
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(rows);
    });
});

router.get('/:deliveryId', (req, res) => {
    const id = req.params.deliveryId;
    let sql = `select * from Delivery where trackingid = ?;`;
    db.get(sql, id, (err, row) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(row);
    });
});

router.post('/', (req, res, next) => {
    var data = new Delivery(req.body.trackingId, req.body.senderId,
        req.body.recipientId, req.body.status, req.body.type, req.body.note);
    var errors = Delivery.deliveryValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `INSERT INTO delivery (trackingId, senderId, recipientId, note, status, type) 
            VALUES (?,?,?,?,?,?);`;
        var params = [data.trackingId, data.senderId, data.recipientId, data.note, data.status, data.type];
        db.run(sql, params, function(err, result) {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.status(201).json(result);
        });
    }
});

router.patch('/:deliveryId', (req, res, next) => {
    var id = req.params.deliveryId;
    var data = new Delivery(id, req.body.senderId,
        req.body.recipientId, req.body.status, req.body.type, req.body.note);
    var errors = Delivery.deliveryValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `UPDATE Delivery set 
            trackingId = ?,
            senderId = ?,
            recipientId = ?,
            note = ?,
            status = ?,
            type = ?
            WHERE id = ?;`;
        var params = [data.trackingId, data.senderId, data.recipientId, data.note, data.status, data.type, id];
        db.run(sql, params, (err, result) => {
            if (err) {
                res.status(500).json({ 'error': res.message });
                return;
            }
            res.json({
                message: 'success',
                data: data,
                changes: this.changes
            });
        });
    }
});

router.delete('/:deliveryId', (req, res, next) => {
    const id = req.params.deliveryId
    let sql = `DELETE FROM Delivery WHERE trackingId = ?;`;
    db.run(sql, id, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message })
            return;
        }
        res.json({ 'message': 'deleted', changes: this.changes })
    });
});

router.get('/details/:trackingId', (req, res) => {
    let sql = `SELECT a.trackingId, b.sentDate, 
    b.dueAmount, a.status, a.type, b.isPayed,
    d.name as recName, d.lastname as recLast,
    d.street as recStreet, e.zipCode as recZip,
    f.city as recCity, d2.name as sendName, 
    d2.lastname as sendLast, d2.street as sendStreet,
    e2.zipCode as sendZip, f2.city as sendCity
    FROM Delivery a
    JOIN Parcel b ON a.trackingId = b.trackingId
    JOIN Users c ON a.senderId = c.id
    JOIN Recipient d ON a.recipientId = d.id
    JOIN Recipient d2 ON c.recipientId = d2.id
    JOIN ZipCode e ON  d.zipCodeId = e.id
    JOIN ZipCode e2 ON d2.zipCodeId = e2.id
    JOIN City f ON e.cityId = f.id
    JOIN City f2 ON e2.cityId = f2.id
    Where a.trackingId = ?`;
    var params = [req.params.trackingId];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(rows);
    });
});

module.exports.route = router;