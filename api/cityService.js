const express = require('express');
const router = express.Router();
const City = require('../model/city');
const db = require('../db/database');


router.get('/', (req, res) => {
    let sql = `SELECT City.id, City.city, Region.region FROM City
        JOIN Region ON City.regionId = Region.id;`;
    var params = [];
    db.all(sql, params, (err, rows) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(rows);
    });
});

router.get('/:cityId', (req, res) => {
    const id = req.params.cityId;
    let sql = `select * from City where id = ?;`;
    db.get(sql, id, (err, row) => {
        if (err) {
            res.status(500).json({ 'error': err.message });
            return;
        }
        res.json(row);
    });
});

router.post('/', (req, res, next) => {
    var data = new City(req.body.city, req.body.regionId);
    var errors = City.cityValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `INSERT INTO City (city, regionId) VALUES (?,?);`;
        var params = [data.city, data.regionId];
        db.run(sql, params, function(err, result) {
            if (err) {
                res.status(500).json({ 'error': err.message });
                return;
            }
            res.status(201).json(result);
        });
    }
});

router.patch('/:cityId', (req, res, next) => {
    var id = req.params.cityId;
    var data = new City(req.body.city, req.body.regionId);
    var errors = City.cityValidator(data);
    if (errors.length) {
        res.status(500).json(errors)
    } else {
        let sql = `UPDATE City set 
            city = ?,
            regionId = ?
            WHERE id = ?;`;
        var params = [data.city, data.regionId, id];
        db.run(sql, params, (err, result) => {
            if (err) {
                res.status(500).json({ 'error': res.message });
                return;
            }
            res.json({
                message: 'success',
                data: data,
                changes: this.changes
            });
        });
    }
});

router.delete('/:cityId', (req, res, next) => {
    const id = req.params.cityId
    let sql = `DELETE FROM City WHERE id = ?;`;
    db.run(sql, id, (err, result) => {
        if (err) {
            res.status(500).json({ 'error': res.message })
            return;
        }
        res.json({ 'message': 'deleted', changes: this.changes })
    });
});

module.exports.route = router;